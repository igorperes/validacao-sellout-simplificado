#!/usr/bin/env python3
# coding: utf-8
# -- Author: Luiz Gustavo Barbosa Souza

import pathlib
import sys 
import json
import shutil, os
import io
import logging
import re
import barcodenumber
import pandas as pd
from setup_logger import setup_logger
from datetime import date, datetime


class base(object):
    def __init__(self):
        super(base, self).__init__()
        self.logger = setup_logger()
        self.directory = os.getcwd()

    def open_json(self, directory, filename):      
        with io.open(str(directory + filename),  encoding="utf-8") as fp:
            try:
                data = json.load(fp)
            except UnicodeError:
                df = pd.read_parquet(str(directory + filename), engine='pyarrow')
                disordered_json  = df.to_dict('records')
                data = self.format_json(disordered_json)
        return data

    def remove_file(self,directory):
        diretorio = pathlib.Path(directory)
        arquivos = diretorio.glob('.gitkeep')
        for arquivo in arquivos:
            if arquivo is not None:
                os.remove(str(arquivo))

    def format_json(self, json_to_format):
        formatted_json = []
        item_read = []
        for i in json_to_format:
            if i['id'] not in item_read:
                order_cover = {
                                "id":i['id'],
                                "total_cupom":i['total_cupom'],
                                "desconto_cupom":i['desconto_cupom'],
                                "acrescimo_cupom":i['acrescimo_cupom'],
                                "frete_cupom":i['frete_cupom'],
                                "flag_cancelamento_cupom":i['flag_cancelamento_cupom'],
                                "id_pdv":i['id_pdv'],
                                "cnpj_emitente":i['cnpj_emitente'],
                                "data_hora_lancamento":i['data_hora_lancamento'],
                                "origem_coleta":i['origem_coleta'],
                                "items":[
                                    {
                                        "id":i['id'],
                                        "sku":i['sku'],
                                        "codigo_interno_item":i['codigo_interno_item'],
                                        "descricao_item":i['descricao_item'],
                                        "quantidade_item":i['quantidade_item'],
                                        "flag_cancelamento_item":i['flag_cancelamento_item'],
                                        "acrescimo_item":i['acrescimo_item'],
                                        "frete_item":i['frete_item'],
                                        "cfop_item":i['cfop_item'],
                                        "desconto_item":i['desconto_item'],
                                        "embalagem":i['embalagem'],
                                        "valor_unitario":i['valor_unitario']
                                        }
                                ]
                                }
                formatted_json.append(order_cover)
                item_read.append(order_cover['id'])
            else:
                for ir in formatted_json:
                    if i['id'] == ir['id']:
                        ir['items'].append({
                                        "id":i['id'],
                                        "sku":i['sku'],
                                        "codigo_interno_item":i['codigo_interno_item'],
                                        "descricao_item":i['descricao_item'],
                                        "quantidade_item":i['quantidade_item'],
                                        "flag_cancelamento_item":i['flag_cancelamento_item'],
                                        "acrescimo_item":i['acrescimo_item'],
                                        "frete_item":i['frete_item'],
                                        "cfop_item":i['cfop_item'],
                                        "desconto_item":i['desconto_item'],
                                        "embalagem":i['embalagem'],
                                        "valor_unitario":i['valor_unitario']
                                        })            
        return formatted_json

    def sku_is_valid(self, sku):
        if sku is not None:
            return barcodenumber.check_code('ean13', sku)    
        else:
            return False
            
    def get_dictonary(self, entity):
        directory = os.getcwd() + '/../dictionaries/'
        filename = entity + '.json'
        return self.open_json(directory, filename)

    def get_json_directory(self, status):
        if status == "processado": 
            return os.getcwd() + '/../jsons/'
        elif status == "nao-processado": 
            return os.getcwd() + '/../jsons/nao-processado/'
        else:
            return None

    def get_path_logging(self, entity, origem_coleta):
        directory = os.getcwd() + '/../log/' + entity + "/" + origem_coleta + "/" + str(date.today()) + "/"
        if not os.path.exists(directory):
            os.makedirs(directory)     
        return  directory + entity + '.log'

    def move_processed_file(self, filename, entity, origem_coleta):
        path_file = self.get_json_directory("nao-processado") + filename
        directory = self.get_json_directory("processado") + origem_coleta + "/" + entity + "/" + str(date.today()) + "/"
        destination_path = directory + filename


        if not os.path.exists(directory):
            os.makedirs(directory)     
        
        if not os.path.exists(destination_path):   
            shutil.move(path_file, destination_path)
        else:
            print ("Destination path '%s' already exists... " % (destination_path))
            shutil.move(path_file, destination_path)


