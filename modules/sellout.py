#!/usr/bin/env python
# coding: utf-8
# /usr/bin/env python2
# -- Author: Luiz Gustavo Barbosa Souza


import barcodenumber
import re
import os
import logging

from json_validator import json_validator 
from base import base

class sellout(json_validator):

    def __init__(self):
        self.entity = "sellout"
        self.key_sellout_list = []
        super(sellout, self).__init__(self.entity)
    
   
    def __validate_addition(self, filename, sum_acrescimo_item, acrescimo_cupom, id):
        if sum_acrescimo_item is not None and acrescimo_cupom is not None:
            diff = round(float(sum_acrescimo_item),2) - round(float(acrescimo_cupom),2)
            if diff == 0.00 and sum_acrescimo_item != 0.0:
                self.logger.error("Filename: %s - ID: %s - O acréscimo do cabeçalho (%s) não pode ser igual ao acréscimo dos itens (%s) .." % (filename, str(id), str(acrescimo_cupom), str(sum_acrescimo_item)))
    
    def __validate_discount(self, filename, sum_desconto_item, desconto_cupom, id):
        diff = round(float(sum_desconto_item),2) - round(float(desconto_cupom),2)
        if  diff == 0.00 and sum_desconto_item != 0.00 and desconto_cupom != 0.00:   
            self.logger.error("Filename: %s - ID: %s - O desconto do cabeçalho (%s) não pode ser igual ao desconto dos itens (%s) .." % (filename, str(id), str(desconto_cupom), str(sum_desconto_item)))
    
    def __validate_subtotal(self, filename, sum_items, count_itens_aprovados, json_sellout):
        missing_attribute = False
        for k in ["desconto_cupom", "acrescimo_cupom", "total_cupom", "flag_cancelamento_cupom", "id"]:
            if k not in json_sellout:
                missing_attribute = True

        if not missing_attribute:
            total_calculado = round(sum_items - float(json_sellout.get('desconto_cupom')) + float(json_sellout.get('acrescimo_cupom')) + float(json_sellout.get('frete_cupom')) ,2)
            diff =  total_calculado - float(json_sellout.get('total_cupom'))
                
             #nota aprovada com items aprovados?
            if ((diff <= -0.2) or (diff >= 0.2)) and str(json_sellout.get('flag_cancelamento_cupom')) == 'N' and count_itens_aprovados != 0:
                 self.logger.error("Filename: %s - ID: %s - O total informado (%s) é diferente do total calculado (%s)." % (filename, str(json_sellout.get('id')), str(json_sellout.get('total_cupom')), str(total_calculado)))
                    
            #nota aprovada sem itens?
            if count_itens_aprovados == 0 and json_sellout['flag_cancelamento_cupom'] == 'N':
                self.logger.error("Filename: %s - ID: %s - A venda não possui itens aprovados. %s" % (filename, str(json_sellout.get('id')), str(json_sellout.get('cnpj_emitente'))))
    
    
    def __validate_duplicate_key(self, sellout):
        missing_attribute = True

        #Para nfe, nfce, sat usar a chave de acesso
        for k in ["id"]:
           if k in sellout:
                missing_attribute = False
                identificador_venda = sellout.get('id')
     
            
        #identificador ja existe?
        if not missing_attribute and identificador_venda in self.key_sellout_list:
             self.logger.error("Venda Duplicada: %s" % (identificador_venda))
        elif not missing_attribute:
            self.key_sellout_list.append(identificador_venda.encode('utf-8'))    
        

    def main(self):
        super(sellout, self).__init__
        json_directory = self.get_json_directory("nao-processado")
        cfop_dictonary = self.get_dictonary("cfop")
        cfop_sellout_list = []
        count_sellout = 0
        count_valid_ean = 0
        count_cod_interno = 0
        count_items = 0

        #carga inicial dos cfops de venda 
        for cfop in cfop_dictonary:
            if cfop.get('tipo') == 'VENDA':
                cfop_sellout_list.append(cfop.get('numero').encode("utf-8"))

        #inicio validacoes de sellout
        self.remove_file(json_directory)

        for filename in os.listdir(json_directory):    
            for json_sellout in self.open_json(json_directory, filename):
                
                #contadores/sumarizadores do sellout
                count_sellout         += 1
                count_itens_aprovados = 0 
                sum_total_items       = 0
                sum_item_addition     = 0
                sum_item_discount     = 0
                sum_installments      = 0

                #items da venda
                if json_sellout.get('items') is not None:
                    for items in json_sellout.get('items'):
                        count_items += 1
                        
                        
                        #validacoes items da venda
                        self.validate_json("sellout_items", filename, items, json_sellout.get('id'))
                        
                        #quantidade de itens com ean
                        if self.sku_is_valid(items.get('sku')):
                            count_valid_ean += 1
                        
                        #validacao cfop do item
                        if items.get('cfop') is not None:
                            if str(items.get('cfop')) not in cfop_sellout_list:
                                 self.logger.error("Filename: %s - ID: %s - CFOP (%s) não é destinado a venda." % (filename, json_sellout.get('id'), items.get('cfop')))

                        #items_total_calculado
                        if items.get('flag_cancelamento_item') == "N":
                            count_itens_aprovados +=1
                            sum_total_items += float(items['valor_unitario']) * float(items['quantidade_item']) + float(items['acrescimo_item']) - float(items['desconto_item'])                        
                            sum_item_discount += float(items['desconto_item'])
                            sum_item_addition += float(items['acrescimo_item'])
                             
                #validacoes cabecalho da venda
                self.validate_json(self.entity, filename, json_sellout, json_sellout.get('id'))
                self.__validate_addition(filename, sum_item_addition, json_sellout.get('acrescimo_cupom'), json_sellout.get('id'))
                self.__validate_discount(filename, sum_item_discount, json_sellout.get('desconto_cupom'), json_sellout.get('id'))
                self.__validate_duplicate_key(json_sellout)
                self.__validate_subtotal(filename, sum_total_items, count_itens_aprovados, json_sellout)

            #movendo json processado para pasta padrao do erp (de acordo com origem_coleta)
            self.move_processed_file(filename, self.entity, json_sellout.get('origem_coleta', json_sellout.get('cnpj_emitente')))       

        #Encerramento
        self.logger.info("Analisado %s vendas, com %s items, sendo que %s possuem eans validos." % (count_sellout, count_items, count_valid_ean))
        
if __name__ == "__main__":
    sellout().main()
   
